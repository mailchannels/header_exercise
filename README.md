# MailChannels Programming Exercise #

## Parsing e-mail Messages ##

For this exercise, you will implement a basic parser for e-mail message headers.
Specifically, you will implement the `MessageParser` interface, which takes
an input stream and allows you to retrieve header values by name.

The specification for the e-mail message format is RFC 5322, available at
<http://tools.ietf.org/html/rfc5322>.  The specification is very dense,
in summary:

1. Messages start with a series of headers, followed by an empty line,
   followed by a message body.
2. A message header consists of a header name, followed by a ':' character,
   followed by the header value.
3. If a line in the header section starts with whitespace, it is a continuation
   of the previous header value.

## Testing your Solution ##

There is a test suite set up in `TestParseHeaders.java`.  It only has
one test case so far - you should add any other tests you think are necessary.


## Suggestions for Completing the Exercise ##

* Feel free to ask any clarifying questions about the assignment, as well
  as questions about the message format, or details of Java or any libraries.
* If you wish to use a non-standard library, you may, but you should be able
  to complete this with only the Java standard library.
* Java 8 API docs are available at <http://docs.oracle.com/javase/8/docs/api/>
* You should start with a basic, or even an incomplete, solution.  We may
  add more requirements or ask more questions if you're done early.
