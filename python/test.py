import unittest
import headerparse


class TestHeaderParse(unittest.TestCase):

    def setUp(self):
        self.messageStream = open('Account Verification.eml', 'rb')

    def tearDown(self):
        self.messageStream.close()

    def test_parseHeader(self):
        headers = headerparse.parseHeaders(self.messageStream)
        self.assertEqual("scottm@mailchannels.com", headers["Delivered-To"])


if __name__ == '__main__':
    unittest.main()
