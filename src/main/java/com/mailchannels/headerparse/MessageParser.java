package com.mailchannels.headerparse;

import java.io.InputStream;
import java.util.Map;

public interface MessageParser {
	public Map<String, Object> parseHeaders(InputStream messageStream);
}
