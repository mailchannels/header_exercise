package com.mailchannels.headerparse;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import junit.framework.TestCase;

public class TestParseHeaders extends TestCase {

	private File messageFile = new File("Account Verification.eml");
	private InputStream messageStream;

	protected void setUp() throws Exception {
		super.setUp();
		messageStream = new FileInputStream(messageFile);
	}

	protected void tearDown() throws Exception {
		super.tearDown();

		messageStream.close();
	}

	public void testParseHeaders() throws Exception {
		Map<String,Object> headers = new MessageParserImpl().parseHeaders(messageStream);

		assertEquals("scottm@mailchannels.com", headers.get("Delivered-To"));
	}
}
